#ifndef WHEEL_ODOMETRY_H
#define WHEEL_ODOMETRY_H

#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Int32MultiArray.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <nav_msgs/Odometry.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/utils.h>
#include <tf/LinearMath/Matrix3x3.h>
#include <actionlib_msgs/GoalID.h>
#include <geometry_msgs/PoseStamped.h>
#include <genius_msgs/ChangeTF.h>

#include <chrono>
#include <thread>
#include <std_msgs/Bool.h>

#define RPM_CONSTANT 0.10472

class WheelOdometry
{
public:
  WheelOdometry(ros::NodeHandle& nh);
  void MasterWheelCallback(const std_msgs::Int32MultiArrayConstPtr &msg);
  void Init();
  void Run();
  void Update();

private:
  ros::NodeHandle nh_;
  ros::Rate rate_;
  std::unique_ptr<tf2_ros::TransformBroadcaster> tfB_;
  ros::Subscriber master_wheel_sub_, imu_sub_;
  ros::ServiceServer change_dims_service_;
  ros::Publisher odom_pub_, goal_cancel_pub_, goal_pub_;
  ros::Time last_time_, last_zero_vel_;
  double number_of_magnets_;
  double gear_;
  double radius_, width_, length_;
  double imu_vel_x_, imu_vel_y_;
  geometry_msgs::TransformStamped transform_msg_;
  double x_, y_, x_distance_, y_distance_;
  double mw_meters_, tr_meters_, bl_meters_, br_meters_;
  double last_mw_meters_, last_tr_meters_, last_bl_meters_, last_br_meters_;
  double prev_x_distance_, prev_y_distance_, x_vel_, y_vel_;
  bool first_callback_, first_vel_mis_;
  ros::Publisher move_pub_;

  void IMUCallback(const sensor_msgs::ImuConstPtr& msg);
  bool ChangeDims(genius_msgs::ChangeTF::Request& req, genius_msgs::ChangeTF::Response& res);

  tf::Quaternion quaternion_;
  // tf2::Quaternion quaternion_;
  double angle_, prev_angle_, ang_vel_, final_angle_;
  double delta_x_, delta_y_, delta_angle_;
  bool use_imu_, publish_odom_;
};

#endif
