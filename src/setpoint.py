#! /usr/bin/env python3


import rospy
import rosparam
from std_msgs.msg import Float32, Float64, Int32, Int32MultiArray
from geometry_msgs.msg import Twist
from math import sin, cos

SPEED_RATE = 8


class SetPoint:

    def __init__(self, radius, length):
        self.left_velocity = rospy.Publisher('/left/set_state', Float32, queue_size=100)
        self.right_velocity = rospy.Publisher('/right/set_state', Float32, queue_size=100)
        self.left_velocity_ramp = rospy.Publisher('/left/ramp_set_state', Float32, queue_size=100)
        self.right_velocity_ramp = rospy.Publisher('/right/ramp_set_state', Float32, queue_size=100)
        self.v_l = Int32()  # Float32()
        self.v_r = Int32()  # Float32()
        self.v_l_bot = Int32()
        self.v_r_bot = Int32()
        self.v_l.data = 0
        self.v_r.data = 0
        self.v_l_bot.data = 0
        self.v_r_bot.data = 0
        self.vel_pub = rospy.Publisher('/rpm', Int32MultiArray, queue_size=1000)

        self.vel_arr = Int32MultiArray()
        self.vel_arr.data = [0, 0, 0, 0]

        self.wheel_radius = 0.05
        self.x_offset = rospy.get_param("/setpoint/x_offset")
        self.y_offset = rospy.get_param("/setpoint/y_offset")
        self.footprint_length = rospy.get_param("/setpoint/length")
        self.footprint_width = rospy.get_param("/setpoint/width")
        if self.x_offset < 0.0:
            self.wheel_seperation_length = self.footprint_length
        else:
            self.wheel_seperation_length = self.footprint_length - (2 * self.x_offset)
        if self.y_offset < 0.0:
            self.wheel_seperation_width = self.footprint_width
        else:
            self.wheel_seperation_width = self.footprint_width - (2 * self.y_offset)

        # rospy.loginfo("The wheel_length: %.2f", self.wheel_seperation_width)
        self.gear_ratio = 21.75
        self.number_of_pair_poles = 1
        self.constant = self.wheel_radius * 0.10472

    def callback(self, msg):
        x = msg.linear.x
        z = msg.angular.z / 2.0
        self.v_r.data = x + (z * self.wheel_seperation_length / 2.0)  # 1 rad/s
        self.v_l.data = x - (z * self.wheel_seperation_length / 2.0)
        self.v_l.data = (self.gear_ratio * self.v_l.data) / self.constant
        self.v_r.data = (self.gear_ratio * self.v_r.data) / self.constant

        self.vel_arr.data[0] = self.v_l.data
        self.vel_arr.data[1] = self.v_r.data
        self.vel_pub.publish(self.vel_arr)

    def callback_mecanum(self, msg):
        x = msg.linear.x
        y = msg.linear.y
        z = msg.angular.z / 2.0
        mecanum_const = z * (self.wheel_seperation_length + self.wheel_seperation_width)
        self.v_l.data = (self.gear_ratio * (x - y - mecanum_const)) / self.constant
        self.v_r.data = (self.gear_ratio * (x + y + mecanum_const)) / self.constant
        self.v_l_bot.data = (self.gear_ratio * (x + y - mecanum_const)) / self.constant
        self.v_r_bot.data = (self.gear_ratio * (x - y + mecanum_const)) / self.constant

        self.vel_arr.data[0] = int(self.v_l.data * self.number_of_pair_poles)
        self.vel_arr.data[1] = int(self.v_r.data * self.number_of_pair_poles)
        self.vel_arr.data[2] = int(self.v_l_bot.data * self.number_of_pair_poles)
        self.vel_arr.data[3] = int(self.v_r_bot.data * self.number_of_pair_poles)
        self.vel_pub.publish(self.vel_arr)
        if x == 0.0 and y == 0.0 and z == 0.0:
            self.vel_pub.publish(self.vel_arr)
            self.vel_pub.publish(self.vel_arr)
            self.vel_pub.publish(self.vel_arr)
            self.vel_pub.publish(self.vel_arr)
            self.vel_pub.publish(self.vel_arr)


if __name__ == '__main__':
    rospy.init_node('setpoint_node')
    set_point = SetPoint(0.03, 0.4)
    rospy.Subscriber('/smooth_cmd_vel', Twist, set_point.callback_mecanum)
    rospy.spin()
