#! /usr/bin/env python3

import rospy
from std_msgs.msg import Float32MultiArray
import tf2_ros
import geometry_msgs.msg
from sensor_msgs.msg import PointCloud2
import sensor_msgs.point_cloud2 as pc2
from nav_msgs.msg import Odometry

x = 0
y = 0


def Callback(msg):

    br = tf2_ros.TransformBroadcaster()
    t = geometry_msgs.msg.TransformStamped()

    t.header.stamp = rospy.Time.now()
    t.header.frame_id = "odom"
    t.child_frame_id = "base_footprint"
    t.transform.translation.x = msg.pose.pose.position.x * 1.053
    t.transform.translation.y = msg.pose.pose.position.y * 1.053
    t.transform.translation.z = 0.0
    t.transform.rotation.x = 0
    t.transform.rotation.y = 0
    t.transform.rotation.z = msg.pose.pose.orientation.z
    t.transform.rotation.w = msg.pose.pose.orientation.w


    br.sendTransform(t)


def PCL(point_cloud):
    global x, y
    for p in pc2.read_points(point_cloud, field_names=("x", "y", "intensity"), skip_nans=True):
        map_x = x + p[0]
        map_y = y + p[1]
        # rospy.loginfo(" x : %f  y: %f", map_x, map_y)
        print(map_x, ",", map_y)


if __name__ == '__main__':
    rospy.init_node('radar_pub')
    rospy.Subscriber('/wheel_odometry/wheel_odom', Odometry, Callback)
    # rospy.Subscriber('/can_node/RScan1', PointCloud2, PCL)
    rospy.spin()
