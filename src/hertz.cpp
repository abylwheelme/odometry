//
// Created by parallels on 3/17/20.
//

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>

geometry_msgs::Twist t;

void callback(const geometry_msgs::TwistConstPtr& msg)
{
  t = *msg.get();
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "hertz");
  ros::NodeHandle nh;
  ros::Rate rate(7);

  ros::Subscriber sub = nh.subscribe("/spacenav/twist", 10, callback);

  ros::Publisher pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 1);

  ros::AsyncSpinner spinner(2);
  spinner.start();

  while (ros::ok())
  {
    if (t.linear.x == 0.0 && t.angular.z == 0.0)
    {
      continue;
    }
    else
    {
      pub.publish(t);
    }
    rate.sleep();
  }
}