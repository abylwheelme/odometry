
#include <odometry/wheel_odometry.h>

WheelOdometry::WheelOdometry(ros::NodeHandle& nh) : nh_{ nh }, rate_{ 20 }
{
  Init();
}

void WheelOdometry::Init()
{
  tfB_ = std::make_unique<tf2_ros::TransformBroadcaster>();
  last_time_ = ros::Time(0);
  master_wheel_sub_ = nh_.subscribe("/can_node/tachometer", 10, &WheelOdometry::MasterWheelCallback, this);
  odom_pub_ = nh_.advertise<nav_msgs::Odometry>("wheel_odom", 10);
  move_pub_ = nh_.advertise<std_msgs::Int32MultiArray>("/rpm", 10);
  goal_cancel_pub_ = nh_.advertise<actionlib_msgs::GoalID>("/move_base/cancel", 10);
  goal_pub_ = nh_.advertise<geometry_msgs::PoseStamped>("/move_base_simple/goal", 10);
  change_dims_service_ = nh_.advertiseService("/odometry/change_tf", &WheelOdometry::ChangeDims, this);

  double x_offset, y_offset, cart_length, cart_width;
  nh_.param("x_offset", x_offset, 0.0);
  nh_.param("y_offset", y_offset, 0.0);
  nh_.param("length", cart_length, 2.1);
  nh_.param("width", cart_width, 1.04);
  nh_.param("use_imu", use_imu_, false);
  nh_.param("publish_odom", publish_odom_, true);

  if (use_imu_)
    imu_sub_ = nh_.subscribe("/imu/data", 10, &WheelOdometry::IMUCallback, this);

  if (x_offset < 0)
    length_ = cart_length;
  else
    length_ = cart_length - 2 * x_offset;
  if (y_offset < 0)
    width_ = cart_width;
  else
    width_ = cart_width - 2 * y_offset;

  length_ /= 2;
  width_ /= 2;
  prev_x_distance_ = 0.0;
  prev_y_distance_ = 0.0;
  x_vel_ = 0.0;
  y_vel_ = 0.0;
  imu_vel_x_, imu_vel_y_ = 0;

  ROS_INFO("Length: %.2f", length_);

  nh_.param<double>("number_of_magnets", number_of_magnets_, 7);
  nh_.param<double>("radius", radius_, 0.05);

  gear_ = 21.75;

  x_ = y_ = mw_meters_ = tr_meters_ = bl_meters_ = br_meters_ = 0.0;
  last_mw_meters_ = last_tr_meters_ = last_bl_meters_ = last_br_meters_ = 0.0;
  x_distance_ = y_distance_ = 0.0;
  angle_ = 0.0;
  prev_angle_ = 0.0;
  delta_angle_ = 0.0;
  prev_x_distance_ = prev_y_distance_ = 0.0;
  first_callback_ = true;
  quaternion_.setRPY(0, 0, 0);
  angle_ = 0;
  delta_x_ = delta_y_ = 0;
  ang_vel_ = 0;
  last_zero_vel_ = ros::Time::now();
  first_vel_mis_ = false;
  Run();
}

void WheelOdometry::Run()
{
  ros::AsyncSpinner s(4);  // Use 4 threads
  s.start();
  while (ros::ok())
  {
    Update();
    rate_.sleep();
  }
}

void WheelOdometry::Update()
{
  double vel_dt = 0.05;

  x_distance_ = (mw_meters_ + tr_meters_ + bl_meters_ + br_meters_) / 4.0;
  y_distance_ = (0 - mw_meters_ + tr_meters_ + bl_meters_ - br_meters_) / 4.0;
  angle_ = (-mw_meters_ + tr_meters_ - bl_meters_ + br_meters_) / (2 * (width_ + length_));

  if (first_callback_)
  {
    prev_x_distance_ = x_distance_;
    prev_y_distance_ = y_distance_;
    prev_angle_ = angle_;
    first_callback_ = false;
  }

  if (abs(x_distance_ - prev_x_distance_) > 1)
  {
    prev_x_distance_ = x_distance_;
    ROS_ERROR("x_distance!");
  }
  if (abs(y_distance_ - prev_y_distance_) > 1)
  {
    prev_y_distance_ = y_distance_;
    ROS_ERROR("y_distance!");
  }


  delta_x_ = x_distance_ - prev_x_distance_;
  delta_y_ = y_distance_ - prev_y_distance_;
  delta_angle_ = (angle_ - prev_angle_) / 2.0;
  prev_x_distance_ = x_distance_;
  prev_y_distance_ = y_distance_;
  prev_angle_ = angle_;

  x_vel_ = delta_x_ / vel_dt;
  y_vel_ = delta_y_ / vel_dt;
  ang_vel_ = delta_angle_ / vel_dt;


  first_vel_mis_ = false;
  final_angle_ += delta_angle_;
  double roll, pitch;

  if (!use_imu_)
    quaternion_.setRPY(0,0,final_angle_);

  tf::Matrix3x3(quaternion_).getRPY(roll, pitch, final_angle_);
  x_ += delta_x_ * cos(final_angle_) - delta_y_ * sin(final_angle_);
  y_ += delta_x_ * sin(final_angle_) + delta_y_ * cos(final_angle_);


  nav_msgs::Odometry odom;
  odom.header.stamp = ros::Time::now();
  odom.header.frame_id = "odom";
  odom.pose.pose.position.x = x_;
  odom.pose.pose.position.y = y_;
  odom.pose.pose.position.z = 0;
  odom.pose.pose.orientation.x = quaternion_.x();  // quaternion_.x();
  odom.pose.pose.orientation.y = quaternion_.y();  // q_[1];
  odom.pose.pose.orientation.z = quaternion_.z();  // q_[2];
  odom.pose.pose.orientation.w = quaternion_.w();  // q_[3];
  odom.child_frame_id = "base_footprint";
  odom.twist.twist.linear.x = x_vel_;
  odom.twist.twist.linear.y = y_vel_;
  odom.twist.twist.linear.z = 0;
  odom.twist.twist.angular.z = ang_vel_;

  transform_msg_.header.stamp = ros::Time::now();
  transform_msg_.child_frame_id = "base_footprint";
  transform_msg_.header.frame_id = "odom";
  transform_msg_.transform.translation.x = x_;
  transform_msg_.transform.translation.y = y_;
  transform_msg_.transform.translation.z = 0;
  transform_msg_.transform.rotation.x = quaternion_.x();  // q_[0];
  transform_msg_.transform.rotation.y = quaternion_.y();  // q_[1];
  transform_msg_.transform.rotation.z = quaternion_.z();  // q_[2];
  transform_msg_.transform.rotation.w = quaternion_.w();  // q_[3];

  if (publish_odom_)
  {
    odom_pub_.publish(odom);
    tfB_->sendTransform(transform_msg_);
  }


}

bool WheelOdometry::ChangeDims(genius_msgs::ChangeTF::Request& req, genius_msgs::ChangeTF::Response& res)
{
  length_ = req.wheel_x_sep/2;
  width_ = req.wheel_y_sep/2;
  return true;
}

void WheelOdometry::IMUCallback(const sensor_msgs::ImuConstPtr& msg)
{
  tf2::Quaternion q;
  q.setX(msg->orientation.x);
  q.setY(msg->orientation.y);
  q.setZ(msg->orientation.z);
  q.setW(msg->orientation.w);
  double yaw;
  yaw = tf2::getYaw(q);
  quaternion_.setRPY(0, 0, yaw);
}

void WheelOdometry::MasterWheelCallback(const std_msgs::Int32MultiArrayConstPtr& msg)
{
  double mw, tr, bl, br, diff[4];

  mw = (static_cast<double>(msg->data[0]) / number_of_magnets_ / 6.0 / gear_) * (2.0 * M_PI * radius_) * 1.053;
  tr = (static_cast<double>(msg->data[1]) / number_of_magnets_ / 6.0 / gear_) * (2.0 * M_PI * radius_) * 1.053;
  br = (static_cast<double>(msg->data[3]) / number_of_magnets_ / 6.0 / gear_) * (2.0 * M_PI * radius_) * 1.053;
  bl = (static_cast<double>(msg->data[2]) / number_of_magnets_ / 6.0 / gear_) * (2.0 * M_PI * radius_) * 1.053;

  diff[0] = mw - last_mw_meters_;
  diff[1] = tr - last_tr_meters_;
  diff[2] = bl - last_bl_meters_;
  diff[3] = br - last_br_meters_;

  if (abs(diff[0]) > 0.05)
  {
    diff[0] = 0;
  }
  if (abs( diff[1]) > 0.05)
  {
    diff[1] = 0;
  }
  if (abs( diff[2]) > 0.05)
  {
    diff[2] = 0;
  }
  if (abs(diff[3]) > 0.05)
  {
    diff[3] = 0;
  }

  mw_meters_ += diff[0];
  tr_meters_ += diff[1];
  bl_meters_ += diff[2];
  br_meters_ += diff[3];

  last_mw_meters_ = mw;
  last_tr_meters_ = tr;
  last_bl_meters_ = bl;
  last_br_meters_ = br;



}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "wheel_odom_node");
  ros::NodeHandle nh("~");
  WheelOdometry wheelOdometry(nh);
  /*ros::AsyncSpinner s(4);  // Use 4 threads
  s.start();
  ros::waitForShutdown();*/
}
