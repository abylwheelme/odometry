//
// Created by abyl on 29.08.19.
//

#include <ros/ros.h>
#include <cmath>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/Pose.h>

double normalize(double&& angle)
{
  if (angle > M_PI)
    return M_PI - angle;
  else if (angle < -M_PI)
    return abs(M_PI + angle);
}

class MapTF
{
  double x_;
  double y_;
  double yaw_;
  double thresh_;
  tf::TransformListener listener_;
  ros::NodeHandle nh_;
  ros::Subscriber sub_;
  ros::Subscriber sub1_;

public:
  MapTF(const ros::NodeHandle& nh) : x_{ 0 }, y_{ 0 }, nh_{ nh }
  {
    sub1_ = nh_.subscribe("/initialpose", 1, &MapTF::poseCallback, this);
    // sub_ = nh_.subscribe("/pf/position", 10, &MapTF::poseCallbackInitPose, this);
  }

  double getYaw(tf::Pose& t)
  {
    double yaw, pitch, roll;
    t.getBasis().getEulerYPR(yaw, pitch, roll);
    return yaw;
  }

  void loop()
  {
    static tf::TransformBroadcaster br;
    tf::Transform transform;
    tf::StampedTransform stransform;
    tf::Quaternion q;
    ros::Rate r(0.5);
    while (ros::ok())
    {
      transform.setOrigin(tf::Vector3(x_, y_, 0));
      q.setRPY(0, 0, yaw_);
      transform.setRotation(q);
      br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "map", "odom"));
      r.sleep();
      ros::spinOnce();
    }
  }

  void poseCallback(const geometry_msgs::PoseWithCovarianceStampedConstPtr& msg)
  {
    tf::StampedTransform transform;
    try
    {
      listener_.lookupTransform("odom", "base_footprint", ros::Time(0), transform);
    }
    catch (tf::TransformException& ex)
    {
      ROS_ERROR("map_tf exception: %s", ex.what());
      ros::Duration(1.0).sleep();
    }

    x_ = msg->pose.pose.position.x;
    y_ = msg->pose.pose.position.y;
    yaw_ = tf::getYaw(msg->pose.pose.orientation);
    // float th = tf::getYaw(transform.getRotation());
    // if (yaw_ > th) yaw_ = normalize(yaw_ - th);
    // else yaw_ = normalize(th - yaw_);
  }

  void poseCallbackInitPose(const geometry_msgs::PoseConstPtr& msg)
  {
    tf::StampedTransform transform;
    try
    {
      listener_.lookupTransform("odom", "base_footprint", ros::Time(0), transform);
    }
    catch (tf::TransformException& ex)
    {
      ROS_ERROR("map_tf exception: %s", ex.what());
      ros::Duration(1.0).sleep();
    }

    x_ = msg->position.x - transform.getOrigin().getX();
    y_ = msg->position.y - transform.getOrigin().getY();
    yaw_ = tf::getYaw(msg->orientation) - tf::getYaw(transform.getRotation());
    // float th = tf::getYaw(transform.getRotation());
    // if (yaw_ > th) yaw_ = normalize(yaw_ - th);
    // else yaw_ = normalize(th - yaw_);
  }
};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "map_tf");
  tf::TransformListener listener;
  ros::NodeHandle node;
  MapTF mapTf(node);
  mapTf.loop();
  return 0;
};
